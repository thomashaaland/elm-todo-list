# Elm ToDo List

This project is a web application that consists of a frontend, a backend, and a mock database server. The application is containerized using Docker and orchestrated using Docker Compose.

## Table of Contents

- [Project Overview](#project-overview)
- [Architecture](#architecture)
- [Prerequisites](#prerequisites)
- [Setup](#setup)
  - [Clone the Repository](#clone-the-repository)
  - [Build and Run the Application](#build-and-run-the-application)
  - [Access the Application](#access-the-application)
- [Usage](#usage)
- [Services](#services)
  - [Frontend](#frontend)
  - [Backend](#backend)
  - [JSON Server](#json-server)
- [Configuration](#configuration)
- [Troubleshooting](#troubleshooting)
- [Future Work](#future-work)
- [Contributing](#contributing)
- [License](#license)

## Project Overview

This project is designed to provide a modular and scalable web application environment. It is composed of three main services:
- **Frontend**: A web client built with Elm.
- **Backend**: A Go-based API server.
- **JSON Server**: A mock database server using `json-server` to simulate a REST API.

All services are containerized using Docker and managed with Docker Compose.

## Architecture

The project is organized into the following directories:

```bash
project-root/
├── frontend/       # Contains the Elm frontend application
├── backend/        # Contains the Go backend application
├── db/             # Contains the JSON data and related configuration
└── docker-compose.yml  # Docker Compose configuration file
```

## Prerequisites

Make sure you have the following installed on your machine:

* [Docker](https://www.docker.com/get-started/)
* [Docker Compose](https://docs.docker.com/compose/gettingstarted/)

## Setup

### Clone the Repository

Clone the repository to your local machine:

```bash
git clone https://gitlab.com/thomashaaland/elm-todo-list.git
cd elm-todo-list
```

### Build and Run the Application

To build and run the application, execute the following command:
¨
```bash
docker-compose up --build
```

This command will build the Docker images for the frontend, backend, and JSON server, and start all services defined in the [compose.yaml](compose.yaml) file.

### Access the Application

Once the services are up and running, you can access the application via the following URLs:

* **Frontend: 'http://localhost:8000'**
* **Backend: 'http://localhost:8080'**
* **JSON Server (Mock API): Not accessible externally**

## Usage

You can interact with the application via the frontend UI or by sending requests directly to the backend or the mock database server.

* **Frontend:** Provides the user interface for interacting with the application.
* **Backend:** Exposes API endpoints for data processing and interaction with the database.
* **JSON Server:** Simulates a REST API for testing and development purposes.

## Services

### Frontend

The frontend service is built using Elm and served by a Python server.

### Backend

The backend service is a Go application that serves as the API server.

### JSON Server

The JSON server is used to mock a database with RESTful API capabilities.

## Configuration

Configuration is handled through the [compose.yaml](compose.yaml) file. You can modify the file to change ports, environment vairables, or other settings as needed.

## Troubleshooting

If you encounter issues, here are some common troubleshooting steps:

* **Rebuild the Containers:** If you encounter unexpected behaviour, try rebuilding the containers.
```bash
docker-compose up --build
```

* **Check Logs:** You can view the logs for a specific service using:

```bash
docker-compose logs <service-name>
```

* **Docker Prune:** If you run into disk space issues, consider running:

```bash
docker system prune -f
```

* **Access and browse container:** You log into the container using shell and verify the container:

```bash
docker exec -it <container-name> /bin/sh
```

## Future Work

* **Load Balancing:** Consider setting up a load balancer for the backend service.
* **Cookies:** Implement cookies to make sessions survive page refresh and browser restart.
* **Calendar and deadlines:** Implement calendar and deadlines for the list.
* **Deployment:** Set up CI/CD pipelines for automated deployment.

## Contributing

Contributions are welcome! Please fork the repository and submit a pull request for any changes you'd like to see.

## Licence

This project is licensed under the MIT License. See the [LICENSE](License.txt) file for details.