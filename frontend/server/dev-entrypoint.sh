#!/bin/sh
set -e

BACKEND_URL="${BACKEND_URL:-http://localhost:8080}"

echo "window.env = { BACKEND_URL: \"$BACKEND_URL\" };" > /app/server/config.js

nginx

cd /app/list-item-app

while true; do
    echo "Recompiling..."
    elm make Main.elm --output=/app/server/main.js
    inotifywait -r -e modify,create,delete /app/list-item-app/Page/Login.elm
done