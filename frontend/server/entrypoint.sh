#!/bin/sh
set -e

cat <<EOF > /app/config.js
window.env = {
    BACKEND_URL: "${BACKEND_URL}"
};
EOF

echo "Generated /app/config.js:"
cat /app/config.js

nginx -g 'daemon off;'

#exec "$@"