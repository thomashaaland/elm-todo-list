import http.server
import socketserver
import os

class RequestHandler(http.server.SimpleHTTPRequestHandler):
    def do_GET(self):
        if self.path != "/" and not os.path.exists(self.path[1:]):
            self.path = "/server/index.html"
        return super().do_GET()

PORT = 8000

Handler = RequestHandler
with socketserver.TCPServer(("", PORT), Handler) as httpd:
    print("Serving at port", PORT)
    httpd.serve_forever()