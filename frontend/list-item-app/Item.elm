module Item exposing 
    ( Item
    , ItemId
    , idParser
    , idToString
    , idEncoder
    , idDecoder
    , itemsDecoder
    , itemDecoder
    , itemEncoder
    , newItemEncoder
    , emptyItem )

import Url.Parser exposing (Parser, custom)
import Json.Decode as Decode exposing (Decoder, string, bool)
import Json.Decode.Pipeline exposing (required)
import Json.Encode as Encode
import Time exposing (Posix)

type alias Item =
    { id : String
    , title : String
    , content : String
    , complete : Bool
    , userid : String
    , createdAt : Posix
    , deadline : Posix
    }

type ItemId 
    = ItemId String

idDecoder : Decoder (ItemId)
idDecoder =
    Decode.map ItemId string

idEncoder : ItemId -> Encode.Value
idEncoder id =
    idToString(id) |> Encode.string

idToString : ItemId -> String
idToString (ItemId id) =
    id

itemsDecoder : Decoder (List Item)
itemsDecoder =
    Decode.list itemDecoder

itemDecoder : Decoder Item
itemDecoder =
    Decode.succeed Item
        |> required "id" string
        |> required "title" string
        |> required "content" string
        |> required "complete" bool
        |> required "userid" string
        |> required "createdAt" (Decode.map Time.millisToPosix Decode.int)
        |> required "deadline" (Decode.map Time.millisToPosix Decode.int)

itemEncoder : Item -> Encode.Value
itemEncoder item =
    Encode.object <|
        List.concat
            [ if item.id == "" then [] else [ ("id", Encode.string item.id ) ]
            , [ ( "title", Encode.string item.title )
              , ( "content", Encode.string item.content )
              , ( "complete", Encode.bool item.complete )
              , ( "userid", Encode.string item.userid )
              , ( "createdAt", Encode.int (Time.posixToMillis item.createdAt))
              , ( "deadline", Encode.int (Time.posixToMillis item.deadline))
              ]
            ]

newItemEncoder : Item -> Encode.Value
newItemEncoder item =
    Encode.object
        [ ( "title", Encode.string item.title )
        , ( "content", Encode.string item.content )
        , ( "complete", Encode.bool item.complete )
        , ( "userid", Encode.string item.userid )
        ]

idParser : Parser (ItemId -> a) a
idParser =
    custom "ITEMID" <|
        \itemId ->
            Just (ItemId itemId)

emptyItem : Item
emptyItem =
    { id = ""
    , title = ""
    , content = ""
    , complete = False
    , userid = ""
    , createdAt = Time.millisToPosix 0
    , deadline = Time.millisToPosix 0
    }