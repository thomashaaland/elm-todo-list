module Page.ChatBot exposing (..)

import Browser.Navigation as Nav
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http
import Json.Encode as Encode
import Json.Decode as Decode
import User exposing (User)
import Route

type alias Model =
    { chatBotURL : String
    , navKey : Nav.Key
    , prompt : String
    --, response : String
    , chatStream : List String
    , error : Maybe String
    , isLoading : Bool
    , currentUser : Maybe User
    }

type alias ChatResponse =
    { text : String
    , generationTime : Float
    , tokenCount : Int
    }

init : String -> Maybe User -> Nav.Key -> (Model, Cmd Msg)
init chatBotURL user navKey =
    let
        model =
            { chatBotURL = chatBotURL
            , navKey = navKey
            , prompt = ""
            --, response = ""
            , chatStream = []
            , error = Nothing
            , isLoading = False
            , currentUser = user
            }
    in
    ( model, Cmd.none )

type Msg
    = UpdatePrompt String
    | SendPrompt
    | GotResponse (Result Http.Error String)
    | Logout

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        UpdatePrompt newPrompt ->
            ( { model | prompt = newPrompt }, Cmd.none )
        SendPrompt ->
            ( { model | chatStream = model.prompt :: model.chatStream, prompt = "", isLoading = True, error = Nothing }
            , generateRequest model
            )
        GotResponse result ->
            case result of
                Ok response ->
                    ( { model | chatStream = response :: model.chatStream, isLoading = False, error = Nothing }
                    , Cmd.none
                    )
                Err error ->
                    ( { model | error = Just (errorToString error), isLoading = False }
                    , Cmd.none
                    )
        Logout ->
            ( { model 
                | prompt = ""
                --, response = ""
                , error = Nothing
                , isLoading = False
                , currentUser = Nothing
                }
              , Route.pushUrl Route.Login model.navKey
            )

generateRequest : Model -> Cmd Msg
generateRequest model =
    Http.post
        { url = model.chatBotURL ++ "/generate"
        , body = Http.jsonBody (encodePrompt model.prompt)
        , expect = Http.expectJson GotResponse responseDecoder
        }

encodePrompt : String -> Encode.Value
encodePrompt prompt =
    Encode.object
        [ ( "prompt", Encode.string prompt )
        , ( "max_tokens", Encode.int 1000 )
        , ( "temperature", Encode.float 0.7 )
        ]

responseDecoder : Decode.Decoder String
responseDecoder =
    Decode.map .text
        (Decode.map3 ChatResponse
            (Decode.field "text" Decode.string)
            (Decode.field "generation_time" Decode.float)
            (Decode.field "token_count" Decode.int)
            )

errorToString : Http.Error -> String
errorToString error =
    case error of
        Http.BadUrl url ->
            "Bad URL: " ++ url
        Http.Timeout ->
            "Request timed out"
        Http.NetworkError ->
            "Network error"
        Http.BadStatus code ->
            "Bad status: " ++ String.fromInt code
        Http.BadBody message ->
            "Bad body: " ++ message

view : Model -> Html Msg
view model =
    let
        parsedMessages = List.map parseMessage model.chatStream
    in
    main_ [ class "grid_container" ]
        [ viewStatusBar model
        , div [ class "grid_main" ]
            [ textarea 
                [ rows 4
                , placeholder "Enter your prompt here..."
                , value model.prompt
                , onInput UpdatePrompt
                , class "large-text-input"
                ] []
            , button 
                [ onClick SendPrompt
                , disabled model.isLoading
                ] 
                [ text
                    (if model.isLoading then
                        "Generating..."
                    else
                        "Generate Response"
                    )
                ]
            , viewError model.error
            , div [ class "chat-container" ]
                [ div [ class "message-list" ]
                    ( List.map viewResponse parsedMessages )
                ]
            ]
        ]

type MessagePart
    = TextPart String
    | CodePart String

parseMessage : String -> List MessagePart
parseMessage message =
    let
        lines = String.split "\n" message
        processLines : List String -> List MessagePart -> Bool -> List MessagePart
        processLines remainingLines acc isInCodeBlock =
            case remainingLines of
                [] ->
                    List.reverse acc
                line :: rest ->
                    if String.startsWith "```" line then
                        if isInCodeBlock then
                            processLines rest acc False
                        else
                            processLines rest acc True
                    else if isInCodeBlock then
                        case acc of
                            (CodePart existingCode) :: remainingAcc ->
                                processLines rest
                                    (CodePart (existingCode ++ "\n" ++ line) :: remainingAcc)
                                    isInCodeBlock
                            _ ->
                                processLines rest (CodePart line :: acc) isInCodeBlock
                    else
                        case acc of
                            (TextPart existingText) :: remainingAcc ->
                                processLines rest
                                    (TextPart (existingText ++ "\n" ++ line) :: remainingAcc)
                                    isInCodeBlock
                            _ ->
                                processLines rest (TextPart line :: acc) isInCodeBlock
    in
    processLines lines [] False



viewStatusBar : Model -> Html Msg
viewStatusBar model =
    let 
        message =
            if model.currentUser == Nothing then
                "Please log in"
            else
                case model.currentUser of
                    Just currentUser ->
                        "Welcome, " ++ currentUser.credentials.userName
                    Nothing ->
                        "Welcome, Anonymous"
        logoutButton =
            if model.currentUser == Nothing then
                []
            else
                [ button [ onClick Logout ] [ text "Log out" ] ]
    in
    div [ class "grid_header" ]
        (( h3 [] [ text message ] ) :: (a [ href "/listitems" ] [ text "Main page" ]) :: logoutButton )

viewError : Maybe String -> Html Msg
viewError maybeError =
    case maybeError of
        Just error ->
            p [] [ text error ]
        Nothing ->
            text ""

viewResponse : List MessagePart -> Html Msg
viewResponse response =
    if List.isEmpty response then
        text ""
    else
        div
            [ class "message" ]
            [ div 
                [ class "message-content" ] 
                ( List.map viewMessagePart response )
            ]

viewMessagePart : MessagePart -> Html Msg
viewMessagePart part =
    case part of
        TextPart rawtext ->
            div [ class "text-content" ] [ text rawtext ]
        CodePart code ->
            div [class "code-block" ] [ text code ]