module Page.ListItems exposing (..)

import Error exposing (buildErrorMessage)
import Browser.Navigation as Nav
import Html exposing (Html, main_, p, a, h2, h3, div, li, ul, td, input, button, text, Attribute)
import Html.Attributes exposing (href, checked, type_, class, id)
import Html.Events exposing (on, onClick, keyCode)
import Http exposing (..)
import Platform.Cmd as Cmd
import RemoteData exposing (WebData)
import Item exposing (Item, itemsDecoder, itemEncoder)
import Json.Decode as Decode
import Json.Decode exposing (Error(..))
import Route
import User exposing (User)
import Time
import Task
import StatusBar exposing (viewStatusBar, Msg)
import Calendar exposing (monthToString)

-- MODEL
type alias Model =
    { items : WebData (List Item)
    , hideCompletedList : Bool
    , navKey : Nav.Key
    , editMode : Maybe Item
    , editedItem : Item
    , currentUser : Maybe User
    , backendURL : String
    , timeZone : Time.Zone
    , currentTime : Time.Posix
    , calendar : Calendar.Model
    --, pivotTime : Time.Posix
    , selectedItemForSelectBox : Maybe Item
    }

init : String -> Maybe User -> Nav.Key -> ( Model, Cmd Msg )
init backendURL user navKey =
    let
        model = 
            { items = RemoteData.NotAsked
            , hideCompletedList = True
            , navKey = navKey
            , editMode = Nothing
            , editedItem = 
                { id = ""
                , title = ""
                , content = ""
                , complete = False
                , userid = ""
                , createdAt = Time.millisToPosix 0
                , deadline = Time.millisToPosix 0
                }
            , currentUser = user
            , backendURL = backendURL
            , timeZone = Time.utc
            , calendar = Calendar.init Time.utc (Time.millisToPosix 0)
            , currentTime = Time.millisToPosix 0
            , selectedItemForSelectBox = Nothing
            }
    in
    case model.currentUser of
        Just currentUser ->
            ( model, Cmd.batch
                [ Task.perform AdjustTimeZone Time.here
                , Task.perform identity
                    (Time.now
                        |> Task.andThen
                            (\time ->
                                Task.succeed (CalendarMsg (Calendar.UpdatePivotTime time))
                                    |> Task.andThen (\_ ->
                                        Task.succeed (Tick time)
                                    )
                            )
                    )
                , getListItems backendURL currentUser 
                ]
            )
        Nothing ->
            ( model, Route.pushUrl Route.Login model.navKey )


type Msg
    = NoOp
    | KeyDown Int
    | StartEdit String
    | CancelEdit
    | SaveEdit
    | ItemSaved ( Result Http.Error String )
    | UpdateEditedItem String
    | ToggleCompletion String
    | ToggleHideCompletedList
    | Delete String
    | ItemDeleted ( Result Http.Error String )
    | SendHttpRequest
    | Logout
    | DataReceived ( WebData (List Item) )
    | AdjustTimeZone Time.Zone
    | Tick Time.Posix
    | CalendarMsg Calendar.Msg
    | ShowSelectBox String

getListItems : String -> User -> Cmd Msg
getListItems backendURL user =
    Http.request
        { method = "GET"
        , headers = [ Http.header "Authorization" (user.token.token ) ]
        , url = backendURL ++ "/items?userid=" ++ user.token.token
        , body = Http.emptyBody
        , expect = itemsDecoder |> Http.expectJson (RemoteData.fromResult >> DataReceived)
        , timeout = Nothing
        , tracker = Nothing
        }

-- UPDATE
update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )
        KeyDown code ->
            case (code, model.editMode ) of
                (13, Just _) ->
                    update SaveEdit model
                (27, Just _) ->
                    update CancelEdit model
                _ ->
                    ( model, Cmd.none )
        StartEdit itemId ->
            let
                selectedItem = case model.items of
                    RemoteData.Success items ->
                        List.head (List.filter (\item -> item.id == itemId) items)
                    _ ->
                        Nothing
            in
            case selectedItem of
                Just item ->
                    ( { model | editMode = Just item, editedItem = item }, Cmd.none )
                Nothing ->
                    ( model, Cmd.none )
        CancelEdit ->
            ( { model | editMode = Nothing }, Cmd.none )
        SaveEdit ->
            case model.currentUser of
                Just currentUser ->
                    ( model, saveItem model.backendURL model.editedItem currentUser.token.token )
                Nothing ->
                    ( model, Cmd.none)
        ShowSelectBox itemId ->
            let
                selectedItem = case model.items of
                    RemoteData.Success items ->
                        List.head (List.filter (\item -> item.id == itemId) items)
                    _ ->
                        Nothing
            in
            case selectedItem of
                Just item ->
                    ( { model | selectedItemForSelectBox = Just item }, Cmd.none )
                Nothing ->
                    ( model, Cmd.none )
        ItemSaved _ ->
            case model.currentUser of
                Just currentUser ->
                    ( { model | editMode = Nothing }, (getListItems model.backendURL currentUser) )
                Nothing ->
                    ( model, Cmd.none )
        UpdateEditedItem newText ->
            let
                currentItem = model.editedItem
            in
            ( { model | editedItem = { currentItem | title = newText } }, Cmd.none )
        ToggleCompletion itemId ->
            let
                selectedItem = case model.items of
                    RemoteData.Success items ->
                        List.head (List.filter (\item -> item.id == itemId) items)
                    _ ->
                        Nothing
            in
            case selectedItem of
                Just item ->
                    case model.currentUser of
                        Just currentUser ->
                            ( model, saveItem model.backendURL { item | complete = not item.complete} currentUser.token.token )
                        Nothing ->
                            ( model, Route.pushUrl Route.Login model.navKey )
                Nothing ->
                    ( model, Cmd.none )
        ToggleHideCompletedList ->
            ( { model | hideCompletedList = not model.hideCompletedList }, Cmd.none )
        Delete itemId ->
            case model.currentUser of
                Just currentUser ->
                    ( model, deleteItem model.backendURL itemId currentUser.token.token )
                Nothing ->
                    ( model, Cmd.none )
        ItemDeleted _ ->
            case model.currentUser of
                Just currentUser ->
                    ( model, (getListItems model.backendURL currentUser) )
                Nothing ->
                    ( model, Route.pushUrl Route.Login model.navKey )
        SendHttpRequest ->
            case model.currentUser of
                Just currentUser ->
                    ( { model | items = RemoteData.Loading }, (getListItems model.backendURL currentUser) )
                Nothing ->
                    ( model, Cmd.none )
        Logout ->
            ( { model 
                | items = RemoteData.NotAsked
                , editMode = Nothing
                , editedItem = 
                    { id = ""
                    , title = ""
                    , content = ""
                    , complete = False
                    , userid = ""
                    , createdAt = Time.millisToPosix 0
                    , deadline = Time.millisToPosix 0 }
                , currentUser = Nothing
                }
              , Route.pushUrl Route.Login model.navKey
            )
        DataReceived response ->
            case response of
                RemoteData.Success _ ->
                    case model.currentUser of
                        Nothing ->
                            ( model, Route.pushUrl Route.Login model.navKey )
                        Just _ ->
                            ( { model | items = response }, Cmd.none )

                RemoteData.Failure _ ->
                    ( { model | items = response }, Cmd.none )
                RemoteData.NotAsked ->
                    ( { model | items = response }, Cmd.none )
                RemoteData.Loading ->
                    ( { model | items = response }, Cmd.none )
        AdjustTimeZone newZone ->
            ( { model | timeZone = newZone }, Cmd.none )
        Tick newTime ->
            ( { model | currentTime = newTime }
            , Cmd.batch [ Task.perform (\time -> CalendarMsg (Calendar.UpdatePivotTime time)) Time.now ] )
        CalendarMsg calendarMsg ->
            case calendarMsg of
                Calendar.UpdatePivotTime newTime ->
                    let
                        calendar = model.calendar
                        newCalendar = { calendar | pivotTime = newTime, currentTime = model.currentTime }
                    in
                    ( { model | calendar = newCalendar }, Cmd.none )

deleteItem : String -> String -> String -> Cmd Msg
deleteItem backendURL itemId token =
    Http.request
        { method = "DELETE"
        , headers = [ Http.header "Authorization" token ]
        , url = backendURL ++ "/items/" ++ itemId
        , body = Http.emptyBody
        , expect = Http.expectString ItemDeleted
        , timeout = Nothing
        , tracker = Nothing
        }

saveItem : String -> Item -> String -> Cmd Msg
saveItem backendURL editedItem token =
    let
        itemUrl =
            backendURL ++ "/items/" ++ editedItem.id
    in
    Http.request
        { method = "PATCH"
        , headers = [  Http.header "Authorization" token ]
        , url = itemUrl
        , body = Http.jsonBody (itemEncoder editedItem)
        , expect = Http.expectString ItemSaved
        , timeout = Nothing
        , tracker = Nothing
        }

-- VIEW
view : Model -> Html Msg
view model =
    let
        --calendarModel =
        --    Calendar.init model.timeZone model.currentTime
        activeTasks =
            case model.items of
                RemoteData.Success items ->
                    items
                _ -> []
    in
    main_ [ id "list_item_page", class "grid_container" ] 
        [ viewStatusBar model.currentUser 
            (\msg -> case msg of
                StatusBar.Logout -> Logout ) 
        , div [ class "grid_main" ] 
            [ viewLoadListOrError model 
            , div [] [ viewSelectBox model ]
            ] 
        , div [ class "grid_right" ]
            [ Calendar.view
                model.calendar
                --calendarModel
                activeTasks
                ShowSelectBox
                CalendarMsg
            ]
        ]

viewLoadListOrError : Model -> Html Msg
viewLoadListOrError model =
    case model.items of
        RemoteData.NotAsked ->
            case model.currentUser of
                Just currentUser ->
                    viewLoad ("Initiating with user " ++ currentUser.credentials.userName ++ "...")
                Nothing ->
                    viewLoad ("No user")
        RemoteData.Loading ->
            viewLoad "Loading..."
        RemoteData.Success items ->
            viewList model items
        RemoteData.Failure httpError ->
            buildErrorMessage httpError |> viewError

viewError : String -> Html Msg
viewError errorMessage =
    let
        errorHeading =
            "Unable to log in"
    in
        div []
            [ h3 [] [ text errorHeading ]
            , text ("Error: " ++ errorMessage )
            ]

viewLoad : String -> Html Msg
viewLoad load =
    div []
        [ h3 [] [ text load ]]

compareDeadline : Item -> Item -> Order
compareDeadline a b =
    let
        aMillis = Time.posixToMillis a.deadline
        bMillis = Time.posixToMillis b.deadline
    in
    if aMillis > bMillis then
        GT
    else if aMillis < bMillis then
        LT
    else
        EQ

viewList : Model -> List Item -> Html Msg
viewList model items =
    let
        isNotCompleted item =
            not item.complete
        isCompleted item =
            item.complete
        sorter = List.sortWith compareDeadline
        notCompleteList = ul [ class "list" ] (List.map (viewItem model.timeZone model.selectedItemForSelectBox) (sorter (List.filter isNotCompleted items)))
        completeList = 
            ul [ class (if model.hideCompletedList then "list hidden" else "list" )] (List.map (viewItem model.timeZone model.selectedItemForSelectBox) (sorter (List.filter isCompleted items)))
        ifEmpty = if List.isEmpty (List.filter isNotCompleted items) then (Html.h4 [] [ text "--Nothing to do--" ]) else text ""
        caretClass = if model.hideCompletedList then "caret" else "caret rotated"
    in
    div [] 
        [ div [class "section_header" ]
            [ h2 [] [ text ("Tasks (" ++ String.fromInt (List.length (List.filter isNotCompleted items)) ++ ")") ]
            , div 
                [ class "icon" ] 
                [ a [ href ("/listitems/new/" ++ String.fromInt (Time.posixToMillis model.currentTime))]
                    [ text "+" ]
                ]
            ]
        , notCompleteList
        , div [ class "section_header" ]
            [ h2 [] [ text ("Finished Tasks (" ++ String.fromInt (List.length (List.filter isCompleted items)) ++ ")") ]
            , div [ class "icon" ] [ button [ class caretClass, onClick ToggleHideCompletedList ] [ text "❱" ]
                ]
            ]
        , completeList
        --, edit
        , ifEmpty
        ]

onKeyDown : (Int -> msg) -> Attribute msg
onKeyDown tagger =
    on "keydown" (Decode.map tagger keyCode)

viewItem : Time.Zone -> Maybe Item -> Item -> Html Msg
viewItem zone selectedItem item =
    let
        isCompleted = 
            item.complete
        selectOnClick msg = onClick (msg item.id)
        itemPath =
            "/listitems/" ++ item.id
        deadlineAtString = posixToDate zone item.deadline
        selected = 
            case selectedItem of
                Just sItem ->
                    if sItem.id == item.id then
                        "selected" -- check output selected string if selected
                    else
                        ""
                Nothing ->
                    ""
    in
    li [ class ("list_item_entry" ++ " " ++ selected), onClick (ShowSelectBox item.id) ]
        [ td [] [ div [ class "icon" ] [ input [ type_ "checkbox", checked isCompleted, onClick (ToggleCompletion item.id) ] [] ] ]
        , td [] [ div [] [ text item.title ] ]
        , td [] [ div [] [ text deadlineAtString ] ]
        , td [] [ div [ class "editLink icon" ] [ a [ href itemPath ] [ text "✎" ] ] ]
        , td [] [ div [ class "icon" ] [ button [ selectOnClick Delete] [ text "✖" ] ] ]
        ]

posixToDate : Time.Zone -> Time.Posix -> String
posixToDate zone time =
    let 
        deadlineAtMonth =
            Time.toMonth zone time |> Calendar.monthToString
        deadlineAtDay =
            Time.toDay zone time |> String.fromInt
        deadlineAtYear =
            Time.toYear zone time |> String.fromInt
    in
    deadlineAtDay ++ " " ++ deadlineAtMonth ++ " " ++ deadlineAtYear

viewSelectBox : Model -> Html Msg
viewSelectBox model =
    case model.selectedItemForSelectBox of
        Just item ->
            div []
                [ h3 [] [ text "Selected Task" ]
                , p [] [ text item.title ]
                , p [] [ text item.content ]
                , p [] [ text (posixToDate model.timeZone item.deadline) ]
                ]
        Nothing ->
            div [] [ h3 [] [ text "Nothings selected"] ]

viewNextDeadline : Model -> Html Msg
viewNextDeadline model =
    case model.items of
        RemoteData.NotAsked ->
            text "Loading..."
        RemoteData.Loading ->
            text "Loading..."
        RemoteData.Success items ->
            div []
                [ h3 [] [ text "Next upcoming deadline" ] 
                ,   if List.isEmpty items then
                        text "No tasks available."
                    else
                        viewNextDeadlineItem model.timeZone model.selectedItemForSelectBox items
                ]
        RemoteData.Failure _ ->
            text "Error loading items."

viewNextDeadlineItem : Time.Zone -> Maybe Item -> List Item -> Html Msg
viewNextDeadlineItem zone selectedItem items =
    let
        isNotCompleted item =
            not item.complete
        nextDeadlineItem = getNextDeadline (List.filter isNotCompleted items)
    in
    case nextDeadlineItem of
        Just item ->
            viewItem zone selectedItem item
        Nothing ->
            text "-- No upcoming deadlines --"

getNextDeadline : List Item -> Maybe Item
getNextDeadline items =
    case items of
        [] -> Nothing
        [item] -> Just item
        item :: rest ->
            case getNextDeadline rest of
                Just nextItem ->
                    if Time.posixToMillis item.deadline < Time.posixToMillis nextItem.deadline then
                        Just item
                    else
                        Just nextItem
                Nothing -> Just item