module Page.CreateUser exposing ( Model, Msg, init, view, update )

import Browser.Navigation as Nav
import Html exposing (Html, h1, p, main_, article, text, input, button)
import Html.Attributes exposing (type_, placeholder, value, class, id)
import Html.Events exposing (onClick, onInput)
import Route
import Http
import User exposing (..)
import RemoteData exposing (WebData)
import Process
import Task
import Error exposing (buildErrorMessage)

type alias Model = 
    { navKey : Nav.Key
    , credentials : Credentials
    , status : WebData String
    , countdown : Int
    , backendURL : String
    }

init : String -> Nav.Key -> ( Model, Cmd Msg )
init backendURL navKey =
    ( initialModel backendURL navKey, Cmd.none )

initialModel : String -> Nav.Key -> Model
initialModel backendURL navKey =
    { navKey = navKey
    , credentials = { userName = "", password = "" }
    , status = RemoteData.NotAsked
    , countdown = 3
    , backendURL = backendURL
    }

view : Model -> Html Msg
view model =
    main_ [ id "create_new_user_page", class "row_container" ]
        [ article 
            [ id "create_new_user_form_page", class "form_page" ]
            [ h1 
                [ class "header" ] 
                [ text "Create new user" ]
            , p
                [ class "inputForm"
                ]
                [ input 
                    [ type_ "text"
                    , onInput StoreUser
                    , value model.credentials.userName
                    , placeholder "Username"
                    ] []            
                , input 
                    [ type_ "text"
                    , onInput StorePassword
                    , value model.credentials.password
                    , placeholder "Password"
                    ] []
                ]
            , button 
                [ type_ "button"
                , onClick CreateUser
                , id "createUserButton"
                ]
                [ text "CREATE USER" ]
            , button 
                [ type_ "button"
                , onClick Cancel
                ]
                [ text "CANCEL" ]
            , p [ class "footText" ]
                [ viewStatus model ]
            ]
        ]

viewStatus : Model -> Html Msg
viewStatus model =
    let
        status = model.status
    in
    case status of
        RemoteData.NotAsked ->
            text "Enter username and password to begin"
        RemoteData.Loading ->
            text "Loading..."
        RemoteData.Failure response ->
            case response of
                Http.BadStatus _ ->
                    text "Username already exists"
                _ ->
                    text ("Failed, something happened: " ++ ( buildErrorMessage response))
        RemoteData.Success _ ->
            text ("Success. Redirecting to login in " ++ String.fromInt model.countdown ++ " seconds")

type Msg 
    = NoOp
    | StoreUser String
    | StorePassword String
    | CreateUser
    | UserCreated ( WebData String)
    | Tick
    | RedirectToLogin
    | Cancel

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    let
        credentials = model.credentials
    in
    case msg of
        NoOp ->
            ( model, Cmd.none )
        Cancel ->
            let
                newCredentials = { credentials | userName = "", password = "" }
            in
            ( { model | credentials = newCredentials }, Route.pushUrl Route.Login model.navKey)
        StoreUser newUsername ->
            let
                newCredentials = { credentials | userName = newUsername }
            in
            ( { model | credentials = newCredentials }, Cmd.none )
        StorePassword newPassword ->
            let
                newCredentials = { credentials | password = newPassword }
            in
            ( { model | credentials = newCredentials }, Cmd.none )
        CreateUser ->
            if credentials.userName /= "" && credentials.password /= "" then
                ( { model | status = RemoteData.Loading }, createUser model.backendURL credentials )
            else
                ( model, Cmd.none )
        UserCreated response ->
            case response of
                RemoteData.Success _ ->
                    ( { model | status = response }, redirectingToLogin Tick )
                RemoteData.NotAsked ->
                    ( { model | status = response }, Cmd.none )
                RemoteData.Failure _ ->
                    ( { model | status = response }, Cmd.none )
                RemoteData.Loading ->
                    ( { model | status = response }, Cmd.none )
        Tick ->
            if model.countdown > 0 then
                ( { model | countdown = model.countdown - 1 }
                , redirectingToLogin Tick
                )
            else
                ( model, redirectingToLogin RedirectToLogin )
        RedirectToLogin ->
            ( model, Route.pushUrl Route.Login model.navKey )

createUser : String -> Credentials -> Cmd Msg
createUser backendURL creds =
    Http.request
        { method = "POST"
        , headers = []
        --, url = backendURL ++ "/createUser"
        , url = backendURL ++ "/user"
        , body = Http.jsonBody (credentialsEncoder creds)
        , expect = Http.expectString (RemoteData.fromResult >> UserCreated)
        , timeout = Nothing
        , tracker = Nothing
        }

redirectingToLogin : Msg -> Cmd Msg
redirectingToLogin msg =
    Process.sleep 1000
        |> Task.attempt (\_ -> msg)