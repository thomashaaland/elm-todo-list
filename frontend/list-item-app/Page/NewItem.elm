module Page.NewItem exposing ( Model, Msg, init, update, view )

import Browser.Navigation as Nav
import Page.EditItem exposing (Msg)
import Html exposing ( .. )
import Html.Attributes exposing (..)
import Html.Events exposing ( onClick, onInput )
import Item exposing (Item, emptyItem)
import Http
import Item exposing (itemDecoder, itemEncoder)
import Route
import Error exposing (buildErrorMessage)
import User exposing (User)
import Iso8601
import Time
import Task
import StatusBar exposing (viewStatusBar)

type alias Model = 
    { navKey : Nav.Key
    , item : Item
    , createError : Maybe String
    , currentUser : Maybe User
    , backendURL : String
    , timeZone : Time.Zone
    }

init : String -> Nav.Key -> Maybe Int -> Maybe User -> ( Model, Cmd Msg )
init backendURL navKey deadline user =
    ( initialModel backendURL navKey deadline user, Task.perform AdjustTimeZone Time.here )

initialModel : String -> Nav.Key -> Maybe Int -> Maybe User -> Model
initialModel backendURL navKey deadline currentUser =
    let
        token =
            case currentUser of
                Just user ->
                    user.token.token
                Nothing ->
                    ""
        initDeadline =
            case deadline of
                Just newDeadline ->
                    Time.millisToPosix newDeadline
                Nothing ->
                    Time.millisToPosix 0
    in
    { navKey = navKey
    , item = { emptyItem | userid = token, deadline = initDeadline }
    , createError = Nothing
    , currentUser = currentUser
    , backendURL = backendURL
    , timeZone = Time.utc
    }

view : Model -> Html Msg
view model =
    div [ class "grid_container" ]
        [ viewStatusBar model.currentUser
            (\msg -> case msg of
                StatusBar.Logout -> Logout
            )
        , div [ class "grid_main" ]
            [ h3 [] [ text "Create List Item" ]
            , viewNewItemForm model
            , viewError model.createError
            ]
        ]

monthNumberFromName : Time.Month -> String
monthNumberFromName month =
    case month of
        Time.Jan -> "1"
        Time.Feb -> "2"
        Time.Mar -> "3"
        Time.Apr -> "4"
        Time.May -> "5"
        Time.Jun -> "6"
        Time.Jul -> "7"
        Time.Aug -> "8"
        Time.Sep -> "9"
        Time.Oct -> "10"
        Time.Nov -> "11"
        Time.Dec -> "12"

viewNewItemForm : Model -> Html Msg
viewNewItemForm model = 
    let
        defaultYear = String.padLeft 4 '0' <| String.fromInt <| Time.toYear model.timeZone model.item.deadline
        defaultMonth = String.padLeft 2 '0' <| monthNumberFromName <| Time.toMonth model.timeZone model.item.deadline
        defaultDay = String.padLeft 2 '0' <| String.fromInt <| Time.toDay model.timeZone model.item.deadline
        defaultDate = defaultYear ++ "-" ++ defaultMonth ++ "-" ++ defaultDay
    in
    Html.form [ class "inputForm" ]
        [ div []
            [ text "Title"
            , br [] []
            , input [ type_ "text", onInput StoreTitle ] []
            ]
        , br [] []
        , div []
            [ text "Content"
            , br [] []
            , textarea [ rows 5, class "large-text-input", onInput StoreContent ] []
            ]
        , br [] []
        , div []
            [ input [ type_ "date", value defaultDate, onInput DatePicked ] []
            ]
        , div []
            [ button [ type_ "button", onClick CreateItem ]
                [ text "Submit" ]
            ]
        , div []
            [ button [ type_ "button", onClick Cancel ]
                [ text "Cancel" ]
            ]
        ]

viewError : Maybe String -> Html msg
viewError maybeError =
    case maybeError of
        Just error ->
            div []
                [ h3 [] [ text "Couldn't create a post at this time." ]
                , text ("Error: " ++ error)
                ]
        Nothing ->
            text ""

type Msg
    = StoreTitle String
    | StoreContent String
    | CreateItem
    | Cancel
    | ItemCreated (Result Http.Error Item)
    | DatePicked String
    | AdjustTimeZone Time.Zone
    | Logout

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        AdjustTimeZone zone ->
            ( { model | timeZone = zone }, Cmd.none )
        StoreTitle title ->
            let
                oldItem =
                    model.item
                updateTitle =
                    { oldItem | title = title }
            in
            ( { model | item = updateTitle }, Cmd.none )
        DatePicked dateString ->
            let
                oldItem = model.item
                date =
                    case Iso8601.toTime dateString of
                        Ok date_ ->
                            Just date_
                        Err _ ->
                            Nothing
                updatedItem = 
                    case date of
                        Just date_ ->
                            { oldItem | deadline = date_ }
                        Nothing ->
                            oldItem
            in
            ( { model | item = updatedItem }, Cmd.none )
        StoreContent content ->
            let
                oldItem =
                    model.item
                updateContent =
                    { oldItem | content = content }
            in
            ( { model | item = updateContent }, Cmd.none )
        CreateItem ->
            ( model, createItem model.backendURL model.item )
        ItemCreated (Ok item) ->
            ( { model | item = item, createError = Nothing }
            , Route.pushUrl Route.Items model.navKey
            )
        ItemCreated (Err error) ->
            ( { model | createError = Just (buildErrorMessage error) }
            , Cmd.none
            )
        Cancel ->
            ( model, Route.pushUrl Route.Items model.navKey
            )
        Logout ->
            ( { model | currentUser = Nothing }, Route.pushUrl Route.Login model.navKey )

createItem : String -> Item -> Cmd Msg
createItem backendURL item =
    Http.request
        { method = "POST"
        , headers = [ Http.header "Authorization" item.userid ]
        , url = backendURL ++ "/items"
        , body = Http.jsonBody (itemEncoder item)
        , expect = Http.expectJson ItemCreated itemDecoder
        , timeout = Nothing
        , tracker = Nothing
        }