module Page.EditItem exposing (Model, Msg, init, update, view)

import Error exposing (buildErrorMessage)
import Browser.Navigation as Nav
import Item exposing (Item, ItemId, itemDecoder, itemEncoder)
import Page.ListItems exposing (Msg)
import Http
import RemoteData exposing (WebData)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Route
import User exposing (User)
import StatusBar exposing (viewStatusBar)
import Time
import Iso8601
import Page.ListItems exposing (Msg(..))
import Task

type alias Model =
    { navKey : Nav.Key
    , item : WebData Item
    , saveError : Maybe String
    , currentUser : Maybe User
    , backendURL : String
    , timeZone : Time.Zone
    }

init : String -> ItemId -> Maybe User -> Nav.Key -> ( Model, Cmd Msg )
init backendURL itemId currentUser navKey =
    let
        model = initialModel backendURL currentUser navKey
        token =
            case currentUser of
                Just user ->
                    user.token.token
                Nothing ->
                    ""
    in
    case model.currentUser of
        Just _ ->
            ( model, Cmd.batch [ fetchItem backendURL itemId token, Task.perform AdjustTimeZone Time.here ] )
        Nothing ->
            ( model, Route.pushUrl Route.Login model.navKey )

initialModel : String -> Maybe User -> Nav.Key -> Model
initialModel backendURL user navKey =
    { navKey = navKey
    , item = RemoteData.Loading
    , saveError = Nothing
    , currentUser = user
    , backendURL = backendURL
    , timeZone = Time.utc
    }

fetchItem : String -> ItemId -> String -> Cmd Msg
fetchItem backendURL itemId token =
    Http.request
        { method = "GET"
        , headers = [ Http.header "Authorization" token ]
        , url = backendURL ++ "/items/" ++ Item.idToString itemId
        , body = Http.emptyBody
        , expect = itemDecoder |> Http.expectJson (RemoteData.fromResult >> ItemReceived)
        , timeout = Nothing
        , tracker = Nothing
        }

type Msg
    = ItemReceived (WebData Item)
    | UpdateTitle String
    | UpdateContent String
    | SaveItem
    | Cancel
    | ItemSaved ( Result Http.Error Item)
    | DatePicked String
    | AdjustTimeZone Time.Zone
    | Logout

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        AdjustTimeZone zone ->
            ( { model | timeZone = zone }, Cmd.none )
        ItemReceived item ->
            ( { model | item = item }, Cmd.none )
        UpdateTitle newTitle ->
            let
                updateTitle =
                    RemoteData.map
                        (\itemData ->
                            { itemData | title = newTitle }
                        )
                        model.item
            in
            ( { model | item = updateTitle }, Cmd.none )
        DatePicked dateString ->
            let
                oldItem = model.item
                date =
                    case Iso8601.toTime dateString of
                        Ok date_ ->
                            Just date_
                        Err _ ->
                            Nothing
                updatedItem =
                    case date of
                        Just date_ ->
                            RemoteData.map
                                (\itemData ->
                                    { itemData | deadline = date_ }
                                )
                                oldItem
                        Nothing ->
                            oldItem
            in
            ( { model | item = updatedItem }, Cmd.none )
        UpdateContent newContent ->
            let
                updateContent =
                    RemoteData.map
                        (\itemData ->
                            { itemData | content = newContent }
                        )
                        model.item
            in
            ( { model | item = updateContent }, Cmd.none )
        SaveItem ->
            case model.currentUser of
                Just user ->
                    ( model, saveItem model.backendURL model.item user.token.token )
                Nothing ->
                    ( model, Cmd.none )
        ItemSaved (Ok itemData) ->
            let
                item =
                    RemoteData.succeed itemData
            in
            ( { model | item = item, saveError = Nothing }, Route.pushUrl Route.Items model.navKey )
        ItemSaved (Err error) ->
            ( { model | saveError = Just (buildErrorMessage error) }, Cmd.none )
        Cancel ->
            ( model, Route.pushUrl Route.Items model.navKey )
        Logout ->
            ( { model | currentUser = Nothing }, Route.pushUrl Route.Login model.navKey )

view : Model -> Html Msg
view model =
    div [ class "grid_container" ]
        [ viewStatusBar model.currentUser 
            (\msg -> case msg of
                StatusBar.Logout -> Logout
            )
        , div [ class "grid_main" ]
            [ h3 [] [ text "Edit List Item" ]
            , viewItem model
            , viewSaveError model.saveError
            ]
        ]

monthNumberFromName : Time.Month -> String
monthNumberFromName month =
    case month of
        Time.Jan -> "1"
        Time.Feb -> "2"
        Time.Mar -> "3"
        Time.Apr -> "4"
        Time.May -> "5"
        Time.Jun -> "6"
        Time.Jul -> "7"
        Time.Aug -> "8"
        Time.Sep -> "9"
        Time.Oct -> "10"
        Time.Nov -> "11"
        Time.Dec -> "12"

viewItem : Model -> Html Msg
viewItem model =
    let
        item = model.item
    in
    case item of
        RemoteData.NotAsked ->
            text ""
        RemoteData.Loading ->
            h3 [] [ text "Loading Item..." ]
        RemoteData.Success itemData ->
            editForm model itemData
        RemoteData.Failure httpError ->
            viewFetchError (buildErrorMessage httpError)

editForm : Model -> Item -> Html Msg
editForm model item =
    let
        selectedYear = String.padLeft 4 '0' <| String.fromInt <| Time.toYear model.timeZone item.deadline
        selectedMonth = String.padLeft 2 '0' <| monthNumberFromName <| Time.toMonth model.timeZone item.deadline
        selectedDay = String.padLeft 2 '0' <| String.fromInt <| Time.toDay model.timeZone item.deadline
        selectedDeadline = selectedYear ++ "-" ++ selectedMonth ++ "-" ++ selectedDay
    in
    Html.form [ class "inputForm" ]
        [ div []
            [ text "Title"
            , br [] []
            , input [ type_ "text", value item.title, onInput UpdateTitle ] []
            ]
        , br [] []
        , div []
            [ text "Content"
            , br [] []
            , textarea [ rows 5, class "large-text-input", onInput UpdateContent ] [ text item.content ]
            ]
        , br [] []
        , div []
            [ input [ type_ "date", value selectedDeadline, onInput DatePicked ] []
            ]
        , div []
            [ button [ type_ "button", onClick SaveItem ]
                [ text "Submit" ]
            ]
        , div []
            [ button [ type_ "button", onClick Cancel ]
                [ text "Cancel" ]
            ]
        ]

viewFetchError : String -> Html Msg
viewFetchError errorMessage =
    let
        errorHeading =
            "Couldn't fetch item"
    in
    div []
        [ h3 [] [ text errorHeading ]
        , text ( "Error: " ++ errorMessage )
        ]

viewSaveError : Maybe String -> Html msg
viewSaveError maybeError =
    case maybeError of
        Just error ->
            div []
                [ h3 [] [ text "Couldn't save item at this time." ]
                , text ( "Error: " ++ error )
                ]
        Nothing ->
            text ""

saveItem : String -> WebData Item -> String -> Cmd Msg
saveItem backendURL item token =
    case item of
        RemoteData.Success itemData ->
            let
                itemUrl =
                    --backendURL ++ "/saveItem?itemid=" ++ itemData.id
                    backendURL ++ "/items/" ++ itemData.id
            in
            Http.request
                { method = "PATCH"
                , headers = [ Http.header "Authorization" token ]
                , url = itemUrl
                , body = Http.jsonBody (itemEncoder itemData)
                , expect = Http.expectJson ItemSaved itemDecoder
                , timeout = Nothing
                , tracker = Nothing
                }
        _ ->
            Cmd.none