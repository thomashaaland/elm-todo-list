module Page.Login exposing ( Model, Msg, init, view, update )

import Browser.Navigation as Nav
import Page.ListItems exposing (Msg)
import Html exposing (Html, h1, text, input, button, a, article, main_, p)
import Html.Attributes exposing (type_, placeholder, href, value, id, class)
import Html.Events exposing (onInput, onClick)
import Route
import Http
import RemoteData exposing (WebData)
import User exposing (..)

type alias Model =
    { navKey : Nav.Key
    , token : WebData (Token)
    , user : User
    , backendURL : String
    }


type Msg
    = StoreUser String
    | StorePassword String
    | AuthenticateUser
    | UserAuthenticated ( WebData Token)

init : String -> Nav.Key -> ( Model, Cmd Msg )
init backendURL navKey =
    ( initialModel backendURL navKey, Cmd.none )

initialModel : String -> Nav.Key -> Model
initialModel backendURL navKey =
    { navKey = navKey
    , user = { credentials = { userName = "", password = "" }, token = { token = "" } }
    , token = RemoteData.NotAsked
    , backendURL = backendURL
    }

view : Model -> Html Msg
view model =
    main_ [ id "login_page", class "row_container" ]
        [ article
            [ id "login_form_page", class "form_page" ]
            [ h1 
                [ class "header" ] 
                [ text "Sign In" ]
            , p
                [ class "inputForm"
                ]
                [ input 
                    [ type_ "text"
                    , onInput StoreUser
                    , value model.user.credentials.userName
                    , placeholder "Username"
                    ] []
                , input 
                    [ type_ "password"
                    , onInput StorePassword
                    , value model.user.credentials.password
                    , placeholder "Password"
                    ] []
                ]
            , button 
                [ type_ "button"
                , onClick AuthenticateUser
                , id "loginButton"
                ]
                [ text "LOGIN" ]
            , p
                [ id "loginFeedbackText", class "feedBackText" ]
                [ case model.token of
                    RemoteData.Failure result ->
                        case result of
                            Http.BadUrl msg ->
                                text ("Bad URL: " ++ msg)
                            Http.Timeout ->
                                text ("Timed out")
                            Http.NetworkError ->
                                text "Network error"
                            Http.BadStatus status ->
                                case status of
                                    401 ->
                                        text "Username or password is incorrect"
                                    _ ->
                                        text ("Something strange happened: " ++ String.fromInt status)
                            Http.BadBody msg ->
                                text ("BadBody: " ++ msg)
                    RemoteData.NotAsked ->
                        text ""
                    RemoteData.Loading ->
                        text "Loading..."
                    RemoteData.Success result ->
                        text result.token
                ]
            , p 
                [ class "footText"
                ]
                [ text "Dont't have an account? Sign up and wake up!!"
                , a [ href "/createuser" ] [text "here" ]
                ]
            ]
        ]

update : Msg -> Model -> ( Model, Cmd Msg, Maybe User )
update msg model =
    let
        user = model.user
        credentials = model.user.credentials
    in
    case msg of
        StoreUser newUserName ->
            let
                newCredentials = { credentials | userName = newUserName }
                newUser = { user | credentials = newCredentials }
            in
            ( { model | user = newUser }, Cmd.none, Nothing )
        StorePassword newPassword ->
            let
                newCredentials = { credentials | password = newPassword }
                newUser = { user | credentials = newCredentials }
            in
            ( { model | user = newUser }, Cmd.none, Nothing )
        AuthenticateUser ->
            if credentials.userName /= "" then
                ( { model | token = RemoteData.Loading }, authenticateUser model, Nothing )
            else
                ( model, Cmd.none, Nothing )
        UserAuthenticated response ->
            case response of
                RemoteData.Success token ->
                    let
                        newUser = { user | token = token }
                    in
                    ( model, Route.pushUrl Route.Items model.navKey, Just newUser )
                RemoteData.Failure _ ->
                    ( { model | token = response }, Cmd.none, Nothing )
                RemoteData.NotAsked ->
                    ( { model | token = response }, Cmd.none, Nothing )
                RemoteData.Loading ->
                    ( { model | token = response }, Cmd.none, Nothing )

authenticateUser : Model -> Cmd Msg
authenticateUser model =
    let
        creds = model.user.credentials
    in
    Http.request
        { method = "POST"
        , headers = []
        --, url = model.backendURL ++ "/getUser"
        , url = model.backendURL ++ "/user/authenticate"
        , body = Http.jsonBody (credentialsEncoder creds)
        , expect = tokenDecoder |> Http.expectJson (RemoteData.fromResult >> UserAuthenticated)
        , timeout = Nothing
        , tracker = Nothing
        }
