module Main exposing (main)

import Browser exposing (Document, UrlRequest)
import Browser.Navigation as Nav
import Url exposing (Url)
import Page.ListItems as ListItems
import Page.EditItem as EditItem
import Page.NewItem as NewItem
import Page.ChatBot as ChatBot
import Page.Login as Login
import Page.CreateUser as CreateUser
import Route exposing (Route)
import Platform.Cmd as Cmd
import Html exposing (..)
import User exposing (User)
import Route exposing (Route(..))

main : Program String Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlRequest = LinkClicked
        , onUrlChange = UrlChanged
        }

type alias Model =
    { route : Route
    , page : Page
    , navKey : Nav.Key
    , currentUser : Maybe User
    , backendURL : String
    }

type Page
    = NotFoundPage
    | LoginPage Login.Model
    | ChatBotPage ChatBot.Model
    | ListPage ListItems.Model
    | EditPage EditItem.Model
    | NewPage NewItem.Model
    | CreateUserPage CreateUser.Model

type Msg
    = LoginPageMsg Login.Msg
    | ChatBotPageMsg ChatBot.Msg
    | ListPageMsg ListItems.Msg
    | LinkClicked UrlRequest
    | UrlChanged Url
    | EditPageMsg EditItem.Msg
    | NewPageMsg NewItem.Msg
    | CreateUserPageMsg CreateUser.Msg

init : String -> Url -> Nav.Key -> ( Model, Cmd Msg )
init backendURL url navKey =
    let
        model =
            { route = Route.parseUrl url
            , page = NotFoundPage
            , navKey = navKey
            , currentUser = Nothing
            , backendURL = backendURL
            }
    in
    initCurrentPage ( model, Cmd.none )

initCurrentPage : ( Model, Cmd Msg ) -> ( Model, Cmd Msg )
initCurrentPage ( model, existingCmds ) =
    let
        ( currentPage, mappedPageCmds ) =
            case model.route of
                Route.NotFound ->
                    ( NotFoundPage, Cmd.none )
                Route.Login ->
                    let
                        ( pageModel, pageCmds ) =
                            Login.init model.backendURL model.navKey
                    in
                    ( LoginPage pageModel, Cmd.map LoginPageMsg pageCmds )
                Route.ChatBot ->
                    let
                        ( pageModel, pageCmds ) =
                            ChatBot.init model.backendURL model.currentUser model.navKey
                    in
                    ( ChatBotPage pageModel, Cmd.map ChatBotPageMsg pageCmds )
                Route.Items ->
                    let
                        ( pageModel, pageCmds ) =
                            ListItems.init model.backendURL model.currentUser model.navKey
                    in
                    ( ListPage pageModel, Cmd.map ListPageMsg pageCmds )
                Route.Item itemId ->
                    let
                        ( pageModel, pageCmds ) =
                            EditItem.init model.backendURL itemId model.currentUser model.navKey
                    in
                    ( EditPage pageModel, Cmd.map EditPageMsg pageCmds )
                Route.NewItem ->
                    let
                        ( pageModel, pageCmds ) =
                            NewItem.init model.backendURL model.navKey Nothing model.currentUser
                    in
                    ( NewPage pageModel, Cmd.map NewPageMsg pageCmds )
                Route.NewItemArg deadline ->
                    let
                        ( pageModel, pageCmds ) =
                            NewItem.init model.backendURL model.navKey (Just deadline) model.currentUser
                    in
                    ( NewPage pageModel, Cmd.map NewPageMsg pageCmds )                    
                Route.CreateUser ->
                    let
                        ( pageModel, pageCmds ) =
                            CreateUser.init model.backendURL model.navKey
                    in
                    ( CreateUserPage pageModel, Cmd.map CreateUserPageMsg pageCmds)
    in
    ( { model | page = currentPage }
    , Cmd.batch [ existingCmds, mappedPageCmds ]
    )

view : Model -> Document Msg
view model =
    { title = "Item List App"
    , body = [ currentView model ]
    }

currentView : Model -> Html Msg
currentView model =
    case model.page of
        NotFoundPage ->
            notFoundView
        ChatBotPage pageModel ->
            ChatBot.view pageModel
                |> Html.map ChatBotPageMsg
        ListPage pageModel ->
            ListItems.view pageModel
                |> Html.map ListPageMsg
        EditPage pageModel ->
            EditItem.view pageModel
                |> Html.map EditPageMsg
        NewPage pageModel ->
            NewItem.view pageModel
                |> Html.map NewPageMsg
        LoginPage pageModel ->
            Login.view pageModel
                |> Html.map LoginPageMsg
        CreateUserPage pageModel ->
            CreateUser.view pageModel
                |> Html.map CreateUserPageMsg
        
notFoundView : Html Msg
notFoundView =
    h3 [] [ text "Oops! The page you requested was not found!" ]

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case ( msg, model.page ) of
        ( ListPageMsg subMsg, ListPage pageModel ) ->
            let
                ( updatedPageModel, updatedCmd ) =
                    ListItems.update subMsg pageModel
            in
            ( { model | page = ListPage updatedPageModel }
            , Cmd.map ListPageMsg updatedCmd
            )
        ( LinkClicked urlRequest, _ ) ->
            case urlRequest of
                Browser.Internal url ->
                    ( model
                    , Nav.pushUrl model.navKey ( Url.toString url )
                    )
                Browser.External url ->
                    ( model
                    , Nav.load url
                    )
        ( UrlChanged url, _ ) ->
            let
                newRoute =
                    Route.parseUrl url
            in
            ( { model | route = newRoute }, Cmd.none )
                |> initCurrentPage
        ( LoginPageMsg subMsg, LoginPage pageModel ) ->
            let
                ( updatedPageModel, updatedCmd, user ) =
                    Login.update subMsg pageModel
            in
            case user of
                Just currentUser ->
                    ( { model | currentUser = Just currentUser, page = LoginPage updatedPageModel }
                    , Cmd.map LoginPageMsg updatedCmd
                    )
                Nothing ->
                    ( { model | page = LoginPage updatedPageModel }
                    , Cmd.map LoginPageMsg updatedCmd 
                    )
        ( ChatBotPageMsg subMsg, ChatBotPage pageModel ) ->
            let
                ( updatedPageModel, updatedCmd ) =
                    ChatBot.update subMsg pageModel
            in
            ( { model | page = ChatBotPage updatedPageModel }
            , Cmd.map ChatBotPageMsg updatedCmd
            )
        ( CreateUserPageMsg subMsg, CreateUserPage pageModel ) ->
            let
                ( updatedPageModel, updatedCmd ) =
                    CreateUser.update subMsg pageModel
            in
            ( { model | page = CreateUserPage updatedPageModel }
            , Cmd.map CreateUserPageMsg updatedCmd )
        ( EditPageMsg subMsg, EditPage pageModel ) ->
            let
                ( updatedPageModel, updatedCmd ) =
                    EditItem.update subMsg pageModel
            in
            ( { model | page = EditPage updatedPageModel }
            , Cmd.map EditPageMsg updatedCmd
            )
        ( NewPageMsg subMsg, NewPage pageModel ) ->
            let
                ( updatedPageModel, updatedCmd ) =
                    NewItem.update subMsg pageModel
            in
            ( { model | page = NewPage updatedPageModel }
            , Cmd.map NewPageMsg updatedCmd
            )
        (_, _) ->
            ( model, Cmd.none )

-- SUBSCRIPTIONS

subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none