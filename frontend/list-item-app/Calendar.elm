module Calendar exposing
    ( Model
    , Msg(..)
    , view
    , init
    , monthToString
    )
    
import Time
import Html exposing (Html)
import StatusBar exposing (Msg)
import Html exposing (div)
import Html.Attributes exposing (class)
import Html exposing (button)
import Html exposing (h2)
import Html exposing (text)
import Html.Events exposing (onClick)
import Html exposing (a)
import Html.Attributes exposing (href)
import RemoteData exposing (RemoteData(..))
import Item exposing (Item)

type alias Model =
    { timeZone : Time.Zone
    , currentTime : Time.Posix
    , pivotTime : Time.Posix
    }

type Msg
    = UpdatePivotTime Time.Posix

init : Time.Zone -> Time.Posix -> Model
init timeZone currentTime =
    { timeZone = timeZone
    , currentTime = currentTime
    , pivotTime = currentTime
    }

view : Model -> List Item -> (String -> msg) -> (Msg -> msg) -> Html msg
view model tasks onTaskSelect toMsg =
    div [ class "calendar_container" ]
        [ viewCalendarHeader model toMsg
        , viewCalendarGrid model tasks onTaskSelect
        ]

viewCalendarHeader : Model -> (Msg -> msg) -> Html msg
viewCalendarHeader model toMsg =
    div [ class "calendar_header" ] 
        [ button [ class "icon", onClick ((decrementMonth model.pivotTime model.timeZone) |> UpdatePivotTime |> toMsg ) ] [ text "⏴"]
        , h2 [] [ text ((monthToString (Time.toMonth model.timeZone model.pivotTime)) ++ " " ++ String.fromInt (Time.toYear model.timeZone model.pivotTime))]
        , button [ class "icon", onClick ((incrementMonth model.pivotTime model.timeZone) |> UpdatePivotTime |> toMsg ) ] [ text "⏵" ]
        ]

viewCalendarGrid : Model -> List Item -> (String -> msg) -> Html msg
viewCalendarGrid model items onTaskSelect =
    let
        selectedMonth = Time.toMonth model.timeZone model.pivotTime
    in
    div [class "calendar"] (List.concat 
        [ 
            [ div [class "calendarheaderWeekday"] [text "MON"]
            , div [class "calendarheaderWeekday"] [text "TUE"]
            , div [class "calendarheaderWeekday"] [text "WED"]
            , div [class "calendarheaderWeekday"] [text "THU"]
            , div [class "calendarheaderWeekday"] [text "FRI"]
            , div [class "calendarheaderWeekday"] [text "SAT"]
            , div [class "calendarheaderWeekday"] [text "SUN"]
            ]
            , ( List.map (viewCalendarDay model items onTaskSelect ) (genCalendar model.pivotTime selectedMonth model.timeZone))
        ])

genCalendar : Time.Posix -> Time.Month -> Time.Zone -> List Time.Posix
genCalendar pivotTime selectedMonth zone =
    let
        firstDay = getFirstDay pivotTime selectedMonth zone
        numDaysInSixWeeks = 6 * 7
    in
    (accrueDays firstDay numDaysInSixWeeks zone)

accrueDays : Time.Posix -> Int -> Time.Zone -> List Time.Posix
accrueDays time counter zone =
    if (counter == 0) then
        []
    else
        time :: ( accrueDays ((Time.posixToMillis time + (24 * 60 * 60 * 1000)) |> Time.millisToPosix) (counter - 1) zone )

viewCalendarDay : Model -> List Item -> (String -> msg) -> Time.Posix -> Html msg
viewCalendarDay model items onTaskSelect time =
    let
        dayInfo =
            if model.currentTime == time then
                "today"
            else if (Time.posixToMillis model.currentTime) < (Time.posixToMillis time) then
                "aftertoday"
            else
                "beforetoday"
        monthInfo =
            if (Time.toMonth model.timeZone time) /= (Time.toMonth model.timeZone model.pivotTime) then
                "offmonth"
            else
                ""
        classDesc = monthInfo ++ " " ++ dayInfo
    in
    div [ class ("calendarbox " ++ classDesc)] 
        [ a [ href ("/listitems/new/" ++ String.fromInt (Time.posixToMillis time)), class "calendarday_content" ] 
                [ text ((String.fromInt (Time.toDay model.timeZone time))) ]
        , ( viewTasksOfTheDay model time items onTaskSelect )
        ]

viewTasksOfTheDay : Model -> Time.Posix -> List Item -> (String -> msg) -> Html msg
viewTasksOfTheDay model time items onTaskSelect =
    let
        isNotCompleted item = not item.complete
        dailytasks = getDailyTasks model.timeZone time (List.filter isNotCompleted items)
    in
    div [ class "calendardailytasks dropdown" ]
        [ div [ class "calendardailytasks dropbtn" ] 
            (List.map (\_ -> div [ class "calendardailytasks dropart" ] [])
                (List.range 1 (min 3 (List.length dailytasks)))
            )
        , div [ class "calendardailytasks dropcontent" ]
            [ div [ class "calendardaylytaskslist" ] (List.map (viewDailyTasks onTaskSelect) dailytasks ) ]
        ]

viewDailyTasks : (String -> msg) -> Item -> Html msg
viewDailyTasks onTaskSelect item =
    button [ class "list_item_name_btn", onClick (onTaskSelect item.id) ] [ text item.title ]

getDailyTasks : Time.Zone -> Time.Posix -> List Item -> List Item
getDailyTasks zone targetTime items =
    let
        targetYear = Time.toYear zone targetTime
        targetMonth = Time.toMonth zone targetTime
        targetDay = Time.toDay zone targetTime
        thisYearsTasks = List.filter (\item -> (Time.toYear zone item.deadline == targetYear)) items
        thisMonthsTasks = List.filter (\item -> (Time.toMonth zone item.deadline == targetMonth)) thisYearsTasks
    in
    List.filter (\item -> (Time.toDay zone item.deadline == targetDay)) thisMonthsTasks


monthToString : Time.Month -> String
monthToString month =
    case month of
        Time.Jan -> "Jan"
        Time.Feb -> "Feb"
        Time.Mar -> "Mar"
        Time.Apr -> "Apr"
        Time.May -> "May"
        Time.Jun -> "Jun"
        Time.Jul -> "Jul"
        Time.Aug -> "Aug"
        Time.Sep -> "Sep"
        Time.Oct -> "Oct"
        Time.Nov -> "Nov"
        Time.Dec -> "Dec"

decrementMonth : Time.Posix -> Time.Zone -> Time.Posix
decrementMonth time zone =
    let
        currentMonth = Time.toMonth zone time
    in
    Time.posixToMillis (getFirstDay time currentMonth zone) - (24 * 60 * 60 * 1000) |> Time.millisToPosix

incrementMonth : Time.Posix -> Time.Zone -> Time.Posix
incrementMonth time zone =
    let
        currentMonth = Time.toMonth zone time
    in
    Time.posixToMillis (getLastDay time currentMonth zone) + (24 * 60 * 60 * 1000) |> Time.millisToPosix

getFirstDay : Time.Posix -> Time.Month -> Time.Zone -> Time.Posix
getFirstDay time month zone =
    if (((Time.toMonth zone time) /= month) && ((Time.toWeekday zone time) == Time.Mon)) then
        time
    else
        getFirstDay ((Time.posixToMillis time - (24 * 60 * 60 * 1000)) |> Time.millisToPosix) month zone

getLastDay : Time.Posix -> Time.Month -> Time.Zone -> Time.Posix
getLastDay time month zone =
    if (((Time.toMonth zone time) /= month) && ((Time.toWeekday zone time) == Time.Mon)) then
        time
    else
        getLastDay ((Time.posixToMillis time + (24 * 60 * 60 * 1000)) |> Time.millisToPosix) month zone
