module User exposing (..)

import Json.Decode as Decode exposing (Decoder, string)
import Json.Encode as Encode
import Json.Decode.Pipeline exposing (required)

type alias User =
    { credentials : Credentials
    , token : Token
    }

type alias Credentials =
    { userName : String
    , password : String
    }

type alias Token = { token : String }

emptyToken : Token
emptyToken = { token = "" }

userDecoder : Decoder User
userDecoder =
    Decode.succeed User
        |> required "credentials" credentialsDecoder
        |> required "token" tokenDecoder

userEncoder : User -> Encode.Value
userEncoder user =
    Encode.object <|
        [ ( "credentials", credentialsEncoder user.credentials ) 
        , ( "token", tokenEncoder user.token )
        ]

credentialsDecoder : Decoder Credentials
credentialsDecoder =
    Decode.succeed Credentials
        |> required "username" string
        |> required "password" string

credentialsEncoder : Credentials -> Encode.Value
credentialsEncoder creds =
    Encode.object <|
        [ ( "username", Encode.string creds.userName )
        , ( "password", Encode.string creds.password )
        ]

tokenDecoder : Decoder Token
tokenDecoder =
    Decode.succeed Token
        |> required "token" string

tokenEncoder : Token -> Encode.Value
tokenEncoder token =
    Encode.object <|
        [ ( "token", Encode.string token.token )]