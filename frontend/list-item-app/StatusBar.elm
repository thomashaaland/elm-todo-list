module StatusBar exposing ( viewStatusBar, Msg(..) )

import User exposing (User)
import Html exposing (Html, button, text, a, div, h3)
import Html.Events exposing (onClick)
import Html.Attributes exposing (class, href)

type Msg 
    = Logout

viewStatusBar : Maybe User -> ( Msg -> parentMsg) -> Html parentMsg
viewStatusBar currentUser toParentMsg =
    let 
        message =
            case currentUser of
                Just user ->
                    "Welcome, " ++ user.credentials.userName
                _ ->
                    "Please log in"
        logoutButton =
            if currentUser == Nothing then
                []
            else
                [ button [ onClick (toParentMsg Logout) ] [ text "Log out" ] ]
    in
    div [ class "grid_header" ]
        (( h3 [] [ text message ] ) :: (a [ class "icon", href "/chatbot/" ] [ text "Chatbot" ]) :: logoutButton )