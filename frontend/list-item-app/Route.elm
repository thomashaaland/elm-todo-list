module Route exposing (Route(..), parseUrl, pushUrl)

import Url exposing (Url)
import Url.Parser exposing (..)
import Item exposing (ItemId)
import Browser.Navigation as Nav
--import Time

type Route
    = NotFound
    | Login
    | Items
    | Item ItemId
    | NewItem
    | NewItemArg Int
    | CreateUser
    | ChatBot

parseUrl : Url -> Route
parseUrl url =
    case parse matchRoute url of
        Just route ->
            route
        Nothing ->
            NotFound

matchRoute : Parser (Route -> a) a
matchRoute =
    oneOf
        [ map Login top
        , map Login (s "login" )
        , map CreateUser ( s "createuser" )
        , map ChatBot ( s "chatbot" )
        , map Items (s "listitems" )
        , map Item ( s "listitems" </> Item.idParser)
        , map NewItem ( s "listitems" </> s "new")
        , map NewItemArg ( s "listitems" </> s "new" </> int)
        ]

pushUrl : Route -> Nav.Key -> Cmd msg
pushUrl route navKey =
    routeToString route
        |> Nav.pushUrl navKey

routeToString : Route -> String
routeToString route =
    case route of
        NotFound ->
            "/not-found"
        Login ->
            "/login"
        CreateUser ->
            "/createuser"
        ChatBot ->
            "/chatbot"
        Items ->
            "/listitems"
        Item itemId ->
            "/listitems/" ++ Item.idToString itemId
        NewItem ->
            "/listitems/new"
        NewItemArg deadline ->
            "/listitems/new/" ++ String.fromInt deadline