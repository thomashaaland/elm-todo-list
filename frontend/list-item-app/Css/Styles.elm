module Css.Styles exposing (..)
import Html.Attributes exposing (style)
import Html exposing (div)



backgroundDiv : List (Html.Attribute msg)
backgroundDiv = 
        [ style "display" "flex"
        , style "justify-content" "center"
        , style "flex-direction" "row"
        , style "align-items" "center"
        , style "height" "100vh"
        , style "background-color" "#DFEFEF"
        ]

columnDiv : List (Html.Attribute msg)
columnDiv =
        [ style "display" "flex"
        , style "flex-direction" "column"
        , style "justify-content" "center"
        , style "align-items" "center"
        , style "padding" "20px"
        , style "box-shadow" "0 4px 8px rgba(0, 0, 0, 0.3)"
        , style "width" "20vw"
        , style "height" "calc(20vw * 1.414)"
        , style "max-width" "500px"
        , style "max-height" "calc(500px * 1.414)"
        , style "min-width" "300px"
        , style "min-height" "calc(300px * 1.414)"
        ]

leftColumnDiv : List (Html.Attribute msg)
leftColumnDiv =
        [ style "color" "black"
        , style "background-color" "white"
        , style "border-radius" "10px 0 0 10px"
        ]

rightColumnDiv : String -> String -> List (Html.Attribute msg)
rightColumnDiv color1 color2 =
        [ style "color" "white"
        , style "background" ("linear-gradient(to right, " ++ color1 ++ ", " ++ color2 ++ ")")
        , style "border-radius" "0 10px 10px 0"
        , style "overflow" "hidden"
        , style "position" "relative"
        ]

rightColumnDecoration : String -> String -> List (Html.Html msg)
rightColumnDecoration color1 color2 =
        [ div 
                [ style "background-color" color1
                , style "width" "120%"
                , style "height" "60%"
                , style "opacity" "10%"
                , style "position" "absolute"
                , style "transform" "rotate(200deg) translateX(80%) translateY(-15%)"
                ]
                []
        , div 
                [ style "background-color" color2
                , style "width" "120%"
                , style "height" "60%"
                , style "opacity" "10%"
                , style "position" "absolute"
                , style "transform" "rotate(20deg) translateX(80%) translateY(-20%)"
                ]
                []
        ]

statusBarDiv : List (Html.Attribute msg)
statusBarDiv =
        [ style "color" "black"
        , style "background-color" "#DFEFEF"
        , style "border-bottom" "1px solid gray"
        , style "padding" "10px"
        , style "overflow" "hidden"
        , style "width" "100vw"
        , style "position" "sticky"
        , style "top" "0"
        , style "z-index" "1000"
        , style "flex-grow" "2"
        ]

headerStyle : List (Html.Attribute msg)
headerStyle = 
        [ style "display" "flex"
        , style "width" "100%"
        , style "justify-content" "space-between"
        , style "align-items" "center"
        , style "font-family" "arial"
        ]

itemListDiv : List (Html.Attribute msg)
itemListDiv = 
        [ style "display" "flex"
        , style "align-self" "flex-start"
        , style "justify-content" "space-between"
        , style "flex-direction" "row"
        , style "height" "100vh"
        , style "width" "100vw"
        , style "background-color" "white"
        ]