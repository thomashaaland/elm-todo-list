from fastapi import FastAPI, HTTPException, BackgroundTasks
from fastapi.middleware.cors import CORSMiddleware
from llama_cpp import Llama
import os
import time
import logging
from pydantic import BaseModel, Field
from typing import Optional
import gc
import uvicorn

# Configure logging
logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)
logger = logging.getLogger(__name__)

# GLOBAL VARIABLES
#MODEL_PATH = "model/zephyr-7b-beta.Q4_0.gguf"
MODEL_PATH = "model/qwen2.5-coder-7b-instruct-q4_k_m.gguf"
CONTEXT_SIZE = 1024

class GenerationRequest(BaseModel):
    prompt: str
    max_tokens: Optional[int] = Field(default=2000, ge=1, le=4096)
    temperature: Optional[float] = Field(default=0.7, ge=0.0, le=2.0)
    top_p: Optional[float] = Field(default=0.9, ge=0.0, le=1.0)
    echo: Optional[bool] = Field(default=False)
    stop: Optional[list[str]] = Field(default=["<|im_stop|>"])

class GenerationResponse(BaseModel):
    text: str
    generation_time: float
    token_count: int
    finish_reason: Optional[str]

class LlamaModel:
    def __init__(self):
        cpu_count = os.cpu_count()
        optimal_threads = max(1, cpu_count -1)

        self.model = Llama(
            model_path=MODEL_PATH, 
            n_ctx=CONTEXT_SIZE,
            n_threads=optimal_threads,
            n_batch=512,
            f16_kv=True,
            embedding=False,
            n_gpu_layers=20,
            vocab_only=False,
            use_mlock=True,
            use_mmap=True,
            verbose=True,
            )
        logger.info(f"Model loaded successfully with context size: {CONTEXT_SIZE}")
    
    def __del__(self):
        if hasattr(self, 'model'):
            del self.model
            gc.collect()

app = FastAPI(
    title="Llama API Service",
    description="REST API for text generation using Llama model",
    version="1.0.0"
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

llm = LlamaModel()

@app.post("/generate", response_model=GenerationResponse)
async def generate_text(request: GenerationRequest, background_tasks: BackgroundTasks):
    try:
        start_time = time.time()

        # Format prompt:
        #formatted_prompt = f"You are a programmer very interested in simple and secure code. ### Instruction: {request.prompt}\n### Response:"
        formatted_prompt = f"""<|im_start|>system
        You are Qwen, a helpful AI assistant who provides detailed, thoughtful responses.<|im_end|>
        <|im_start|>user
        {request.prompt}<|im_end|>
        <|im_start|>assistant"""

        # Generate response
        response = llm.model(
            formatted_prompt,
            max_tokens=request.max_tokens,
            temperature=request.temperature,
            top_p=request.top_p,
            stop=request.stop,
            stream=False,
            repeat_penalty=1.1,
            top_k=40,
        )

        generation_time = time.time() - start_time

        # Extract generated text
        generated_text = response["choices"][0]["text"].strip()
        token_count = response["usage"]["total_tokens"]
        finish_reason = response["choices"][0].get("finish_reason", "unknown")

        logger.info(response)
        background_tasks.add_task(gc.collect)

        return GenerationResponse(
            text=generated_text,
            generation_time=round(generation_time, 2),
            token_count=token_count,
            finish_reason=finish_reason,
        )
    except Exception as e:
        logger.error(f"Generation error: {str(e)}")
        raise HTTPException(status_code=500, detail=str(e))

@app.get("/health")
async def shutdown_event():
    global llm
    if llm:
        del llm
        gc.collect()
    logger.info("Shutting down API server")

if __name__ == "__main__":
    uvicorn.run("__main__:app", host="0.0.0.0", port=8000, reload=False)