package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"path"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"golang.org/x/crypto/bcrypt"
)

type TokenString string

type Claims struct {
	UserID string `json:"user_id"`
	jwt.RegisteredClaims
}

type User struct {
	ID       string `json:"id"`
	UserName string `json:"username"`
	Password string `json:"password"`
}

type Credentials struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type Item struct {
	Id        *string `json:"id,omitempty"`
	Title     string  `json:"title"`
	Content   string  `json:"content"`
	Complete  bool    `json:"complete"`
	Userid    string  `json:"userid"`
	CreatedAt int64   `json:"createdAt"`
	Deadline  int64   `json:"deadline"`
}

type LlamaQuery struct {
	Prompt      string  `json:"prompt"`
	MaxTokens   int     `json:"max_tokens"`
	Temperature float32 `json:"temperature"`
}

type LlamaResponse struct {
	Text           string  `json:"text"`
	GenerationTime float32 `json:"generation_time"`
	TokenCount     int     `json:"token_count"`
	FinishReason   *string `json:"finish_reason,omitempty"`
}

var (
	client = &http.Client{
		Timeout: 10 * time.Second,
	}
	baseURL = func() string {
		url := os.Getenv("DB_URL")
		if url == "" {
			url = "http://json-server:5019"
		}
		return url
	}()
	chatboturl = func() string {
		url := os.Getenv("CHATBOT_URL")
		if url == "" {
			url = "http://chatbot:8002"
		}
		return url
	}()
	jwtKey = func() []byte {
		key := os.Getenv("JWT_SECRET")
		if key == "" {
			key = "my_sample_key"
		}
		return []byte(key)
	}()
)

func itemsHandler(w http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case http.MethodGet:
		getItemsHandler(w, req)
	case http.MethodPost:
		createItemHandler(w, req)
	default:
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
	}
}

func itemsIDHandler(w http.ResponseWriter, req *http.Request) {
	log.Println("IDHandler called")
	switch req.Method {
	case http.MethodGet:
		getItemHandler(w, req)
	case http.MethodDelete:
		deleteItemHandler(w, req)
	case http.MethodPatch:
		saveItemHandler(w, req)
	default:
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
	}
}

func main() {
	log.Printf("BaseURL: %s\n", baseURL)
	mux := http.NewServeMux()

	mux.Handle("/items", validateJWT(http.HandlerFunc(itemsHandler)))
	mux.Handle("/items/", validateJWT(http.HandlerFunc(itemsIDHandler)))

	mux.HandleFunc("/user", createUserHandler)
	mux.HandleFunc("/user/authenticate", getUserHandler)
	mux.HandleFunc("/generate", generateHandler)

	handler := enableCORS(mux)
	if err := http.ListenAndServe(":8080", handler); err != nil {
		log.Printf("Server error: %v", err)
		log.Fatal(err)
	}
}

func enableCORS(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, DELETE, PATCH, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type, Authorization")

		if r.Method == http.MethodOptions {
			w.WriteHeader(http.StatusNoContent)
			return
		}

		next.ServeHTTP(w, r)
	})
}

func getItems(userID string) ([]Item, error) {
	url := fmt.Sprintf("%s/listitems?userid=%s", baseURL, userID)
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("received non-200 response code")
	}

	var items []Item
	err = json.NewDecoder(resp.Body).Decode(&items)
	if err != nil {
		return nil, err
	}

	return items, nil
}

func getItem(itemID string) (*Item, error) {
	url := fmt.Sprintf("%s/listitems/%s", baseURL, itemID)
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("received non-200 response code")
	}

	var item Item
	err = json.NewDecoder(resp.Body).Decode(&item)
	if err != nil {
		return nil, err
	}

	return &item, nil
}

func getUser(username string) ([]User, error) {
	url := fmt.Sprintf("%s/users?username=%s", baseURL, username)
	log.Printf("GetUser url: %s\n", url)
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("received non-200 response code")
	}

	var users []User
	err = json.NewDecoder(resp.Body).Decode(&users)
	if err != nil {
		return nil, err
	}

	return users, nil
}

func askLlama(q LlamaQuery) (*LlamaResponse, error) {
	url := chatboturl + "/generate"
	log.Printf("URL: %s", url)
	//url = "http://localhost:8002/generate"
	json_data, err := json.Marshal(q)
	if err != nil {
		return nil, err
	}

	resp, err := http.Post(url, "application/json", bytes.NewBuffer(json_data))
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("received non-200 response code")
	}

	var result LlamaResponse
	json.NewDecoder(resp.Body).Decode(&result)
	log.Printf("Sending response: %v", result)

	return &result, nil
}

func createUser(username, password string) error {
	url := fmt.Sprintf("%s/users", baseURL)

	hashedPassword, err := HashPassword(password)
	if err != nil {
		return err
	}

	values := &Credentials{
		Username: username,
		Password: hashedPassword,
	}

	json_data, err := json.Marshal(values)
	if err != nil {
		return err
	}

	resp, err := http.Post(url, "application/json", bytes.NewBuffer(json_data))
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusCreated {
		return fmt.Errorf("received non-201 response code")
	}

	var res Credentials
	json.NewDecoder(resp.Body).Decode(&res)

	return nil
}

func authenticateUser(username, password string) (string, error) {
	users, err := getUser(username)
	if err != nil {
		return "", err
	}

	for _, user := range users {
		if CheckPasswordHash(password, user.Password) {
			return user.ID, nil
		}
	}

	return "", fmt.Errorf("invalid username or password")
}

func verifyUniqueUser(username string) error {
	users, err := getUser(username)
	if err != nil {
		return err
	}

	if len(users) > 0 {
		return fmt.Errorf("username already exists")
	}

	return nil
}

func deleteItem(itemID string) error {
	url := fmt.Sprintf("%s/listitems/%s", baseURL, itemID)

	req, err := http.NewRequest("DELETE", url, nil)
	if err != nil {
		return err
	}

	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("received non-200 response code")
	}

	return nil
}

func saveItem(itemID string, item Item) (*Item, error) {
	url := fmt.Sprintf("%s/listitems/%s", baseURL, itemID)

	json_data, err := json.Marshal(item)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("PATCH", url, bytes.NewBuffer(json_data))
	if err != nil {
		return nil, err
	}

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("received non-200 response code")
	}

	var returnItem Item
	err = json.NewDecoder(resp.Body).Decode(&returnItem)
	if err != nil {
		return nil, fmt.Errorf("invalid item saved")

	}

	return &returnItem, nil
}

func createItem(item Item) (*Item, error) {
	url := fmt.Sprintf("%s/listitems", baseURL)

	item.CreatedAt = time.Now().UnixMilli()

	json_data, err := json.Marshal(item)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(json_data))
	if err != nil {
		return nil, err
	}

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 201 {
		return nil, fmt.Errorf("received non-201 response code")
	}

	var returnItem Item
	err = json.NewDecoder(resp.Body).Decode(&returnItem)
	if err != nil {
		return nil, fmt.Errorf("invalid item saved")
	}

	return &returnItem, nil
}

func generateHandler(w http.ResponseWriter, req *http.Request) {
	log.Printf("Got a request\n")
	if methodNotAllowed(w, req, http.MethodPost) {
		return
	}

	var question LlamaQuery
	err := json.NewDecoder(req.Body).Decode(&question)
	if err != nil {
		log.Printf("Unable to decode query: %v\n", err)
		http.Error(w, "Invalid request payload", http.StatusBadRequest)
	}

	response, err := askLlama(question)
	if err != nil {
		log.Printf("Got a malformed response from Llama: %+v\n", err)
		http.Error(w, "Error from Llama", http.StatusInternalServerError)
	}
	jsonResponse(w, http.StatusOK, response)
}

func getUserHandler(w http.ResponseWriter, req *http.Request) {
	if methodNotAllowed(w, req, http.MethodPost) {
		return
	}

	var creds Credentials
	err := json.NewDecoder(req.Body).Decode(&creds)
	if err != nil {
		http.Error(w, "Invalid request payload", http.StatusBadRequest)
		return
	}

	userID, err := authenticateUser(creds.Username, creds.Password)
	if err != nil {
		http.Error(w, "Authentication failed: "+err.Error(), http.StatusUnauthorized)
		return
	}

	token, err := GenerateJWT(userID)
	if err != nil {
		http.Error(w, "Could not generate token", http.StatusInternalServerError)
		return
	}

	//response := map[string]string{"id": userID}
	response := map[string]string{"token": token}
	jsonResponse(w, http.StatusOK, response)
}

func createUserHandler(w http.ResponseWriter, req *http.Request) {
	if methodNotAllowed(w, req, http.MethodPost) {
		return
	}

	var creds Credentials
	err := json.NewDecoder(req.Body).Decode(&creds)
	if err != nil {
		http.Error(w, "Invalid request payload", http.StatusBadRequest)
		return
	}

	err = verifyUniqueUser(creds.Username)
	if err != nil {
		http.Error(w, err.Error(), http.StatusConflict)
		return
	}

	err = createUser(creds.Username, creds.Password)
	if err != nil {
		log.Printf("Failed to create user: %+v\n", err)
		http.Error(w, "Failed to create user", http.StatusInternalServerError)
		return
	}

	log.Printf("Successfully created user %+v\n", creds)
	jsonResponse(w, http.StatusCreated, map[string]string{"message": "User created successfully"})
}

func getItemsHandler(w http.ResponseWriter, req *http.Request) {
	log.Println("GetItems called")
	if methodNotAllowed(w, req, http.MethodGet) {
		return
	}

	//userID := req.URL.Query().Get("userid")
	userID := req.Context().Value("userID").(string)

	items, err := getItems(userID)
	if err != nil {
		w.Header().Set("Content-Type", "application/json")
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	log.Printf("User %s retrieves all owned items", userID)

	jsonResponse(w, http.StatusOK, items)
}

func getItemHandler(w http.ResponseWriter, req *http.Request) {
	if methodNotAllowed(w, req, http.MethodGet) {
		return
	}

	userID := req.Context().Value("userID").(string)

	//itemID := req.URL.Query().Get("itemid")
	itemID := path.Base(req.URL.Path)
	log.Printf("Itemid: %s", itemID)

	item, err := getItem(itemID)
	if err != nil {
		w.Header().Set("Content-Type", "application/json")
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	log.Printf("User %s retrieves item %s", userID, itemID)

	jsonResponse(w, http.StatusOK, item)
}

func deleteItemHandler(w http.ResponseWriter, req *http.Request) {
	if methodNotAllowed(w, req, http.MethodDelete) {
		return
	}

	userID := req.Context().Value("userID").(string)

	//itemID := req.URL.Query().Get("itemid")
	itemID := path.Base(req.URL.Path)
	if itemID == "" {
		http.Error(w, "Item ID is required", http.StatusBadRequest)
		return
	}
	log.Printf("User %s deleted item %s\n", userID, itemID)

	err := deleteItem(itemID)
	if err != nil {
		w.Header().Set("Content-Type", "application/json")
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	jsonResponse(w, http.StatusOK, map[string]string{"message": "Item deleted successfully"})
}

func saveItemHandler(w http.ResponseWriter, req *http.Request) {
	if methodNotAllowed(w, req, http.MethodPatch) {
		return
	}

	userID := req.Context().Value("userID").(string)

	//itemID := req.URL.Query().Get("itemid")
	itemID := path.Base(req.URL.Path)
	log.Printf("itemID: %s", itemID)
	if itemID == "" {
		http.Error(w, "Item ID is required", http.StatusBadRequest)
		return
	}
	log.Printf("User %s saving item %s\n", userID, itemID)

	var item Item
	err := json.NewDecoder(req.Body).Decode(&item)
	if err != nil {
		w.Header().Set("Content-Type", "application/json")
		http.Error(w, "Invalid request payload", http.StatusBadRequest)
		return
	}

	item.Userid = userID

	savedItem, err := saveItem(itemID, item)
	if err != nil {
		w.Header().Set("Content-Type", "application/json")
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	jsonResponse(w, http.StatusOK, savedItem)
}

func createItemHandler(w http.ResponseWriter, req *http.Request) {
	if methodNotAllowed(w, req, http.MethodPost) {
		return
	}

	userID := req.Context().Value("userID").(string)

	var item Item
	err := json.NewDecoder(req.Body).Decode(&item)
	if err != nil {
		w.Header().Set("Content-Type", "application/json")
		http.Error(w, "Invalid request payload", http.StatusBadRequest)
		return
	}

	item.Userid = userID

	createdItem, err := createItem(item)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	log.Printf("User %s created a new item", userID)

	jsonResponse(w, http.StatusCreated, createdItem)
}

func jsonResponse(w http.ResponseWriter, statusCode int, payload interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	json.NewEncoder(w).Encode(payload)
}

func methodNotAllowed(w http.ResponseWriter, r *http.Request, allowedMethods string) bool {
	if r.Method != allowedMethods {
		http.Error(w, "Invalid request method", http.StatusMethodNotAllowed)
		return true
	}
	return false
}

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	return string(bytes), err
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func GenerateJWT(userID string) (string, error) {
	expirationTime := time.Now().Add(1 * time.Hour)

	claims := &Claims{
		UserID: userID,
		RegisteredClaims: jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(expirationTime),
			Issuer:    "test",
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(jwtKey)
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

func validateJWT(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		tokenString := r.Header.Get("Authorization")

		if tokenString == "" {
			log.Println("Tokenstring is empty")
			http.Error(w, "Authorization header missing", http.StatusUnauthorized)
			return
		}

		claims := &Claims{}
		token, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
			return jwtKey, nil
		})

		if err != nil {
			log.Printf("Invalid token, got error %v", err)
			http.Error(w, "Invalid token", http.StatusUnauthorized)
			return
		}

		if !token.Valid {
			log.Println("Invalid token")
			http.Error(w, "Invalid token", http.StatusUnauthorized)
			return
		}

		ctx := context.WithValue(r.Context(), "userID", claims.UserID)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
