module todo-list-backend

go 1.19

require golang.org/x/crypto v0.31.0

require github.com/golang-jwt/jwt/v5 v5.2.1
